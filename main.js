'use strict';

const { app, BrowserWindow } = require('electron')
const path = require('path')
const url = require('url')

let mainWindow
let dev = false

if ( process.defaultApp || /[\\/]electron-prebuilt[\\/]/.test(process.execPath) || /[\\/]electron[\\/]/.test(process.execPath)) {
	dev = true
}

if (process.platform === 'win32') {
	app.commandLine.appendSwitch('high-dpi-support', 'true')
	app.commandLine.appendSwitch('force-device-scale-factor', '1')
}

function createWindow() {

	mainWindow = new BrowserWindow({
		width: 1200,
		height: 700,
		show: false,
		frame: false
	})

	let indexPath

	if (dev && process.argv.indexOf('--noDevServer') === -1) {
		indexPath = url.format({
			protocol: 'http:',
			host: 'localhost:8080',
			pathname: 'index.html',
			slashes: true
		})
	} else {
		indexPath = url.format({
			protocol: 'file:',
			pathname: path.join(__dirname, 'dist', 'index.html'),
			slashes: true
		})
	}

	mainWindow.loadURL(indexPath)

	mainWindow.once('ready-to-show', () => {
		mainWindow.show()

	// Open the DevTools automatically if developing
	// if (dev) {
	//   mainWindow.webContents.openDevTools()
	// }
	})

	mainWindow.on('closed', function() {
		mainWindow = null
	})
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
	createWindow()
	const reactDevTools = '/GIT/quantum/node_modules/electron-react-devtools'
	BrowserWindow.addDevToolsExtension(reactDevTools)
})

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow()
	}

})
