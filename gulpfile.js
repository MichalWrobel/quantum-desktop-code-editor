const gulp = require('gulp');
const sass = require('gulp-sass');

const scssFiles = './src/styles/scss/**/*.scss', cssFiles = './src/styles/';

gulp.task('styles', function() {
	return gulp.src(scssFiles)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(cssFiles));
});
gulp.task('watch',function() {
	gulp.watch(scssFiles,['styles']);
});