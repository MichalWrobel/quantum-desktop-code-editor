import fs from 'fs';

const settingsPath = './src/settings.json';
const userSettingsPath = './src/user-settings.json';
const langPath = './src/lang/lang.json';

const settingsHelper = {
	memoryServer: [],
	loadOnStart: () => {
		const mainSettings = settingsHelper.readAll('settings');
		const userSettings = settingsHelper.readAll('user');
		const wantedLanguage = userSettings.hasOwnProperty('lang') ? userSettings.lang : mainSettings.lang;
		const langFile = JSON.parse(fs.readFileSync(langPath, 'utf-8', (error, data) => {
			if(error) {
				console.error('ERROR: File system error #0 (loadOnStart)');
				throw error;
			}
		}));
		if (langFile.hasOwnProperty(wantedLanguage)) {
			settingsHelper.memoryLanguage = langFile[wantedLanguage];
		} else {
			settingsHelper.memoryLanguage = langFile.en;
		}
		settingsHelper.memorySettings = mainSettings;
		settingsHelper.memoryUserSettings = userSettings;
		settingsHelper.memoryEditorSettings = mainSettings.editor;
	},
	readAll: (file) => {
		const path = file === 'user' ? userSettingsPath : settingsPath;
		const settings = fs.readFileSync(path, 'utf-8', (error, data) => {
			if(error) {
				console.error('ERROR: File system error #1 (readAll)');
				throw error;
			}

			return data;
		});
		const parsedData = JSON.parse(settings);

		return parsedData;
	},

	read: (setting) => {
		let settings = settingsHelper.memoryUserSettings;

		let matchingSetting = settingsHelper.match(setting, settings);

		if (matchingSetting === null) {
			settings = settingsHelper.memorySettings

			matchingSetting = settingsHelper.match(setting, settings);
		}
		// const parsedSettings = typeof settings === 'object' ? settings : JSON.parse(settings);

		return settings[matchingSetting];
	},
	write: (setting) => {
		fs.writeFile(userSettingsPath, JSON.stringify(setting, null, '\t'), (error) => {
			if (error) {
				console.error('ERROR: File system error #3 (write)');
				throw error;
			}
		});
	},
	setValue: (value, setting) => {
		// TO DO:  Prevent copying all settings from main settings file.
		let settingsList = settingsHelper.memoryUserSettings;
		let matchValue = settingsHelper.match(setting, settingsList);

		if (matchValue !== null) {
			settingsHelper.memoryUserSettings[matchValue] = value;
		} else {
			settingsList = settingsHelper.memorySettings;
			matchValue = settingsHelper.match(setting, settingsList);

			if (matchValue !== null) {
				settingsHelper.memorySettings[matchValue] = value;
			} else {
				console.error(`ERROR: File system error #5 (setValue) | Cannot find requesteed setting (${setting}) and set its value to (${value})`)
			}
		}

	},
	match: (setting, settings) => {
		const settingsList = Object.keys(settings);

		for(const set in settingsList) {
			if (settingsList[set] === setting) {

				return settingsList[set];
			}
		}

		return null;
	},
	writeFromMemory: () => {
		settingsHelper.write(settingsHelper.memoryUserSettings);
	}
};

export default settingsHelper;