import settingsHelper from '../settingsHelper'

const Lang = {
	translate: (label) => {
		const output = settingsHelper.memoryLanguage[label];

		if (output === '' || typeof(output) !== 'string') {
			console.error('ERROR: Language file error #1 |', `Label used: ${label} |`);

			return 'ERROR';
		}

		return output;
	}
};

export default Lang;