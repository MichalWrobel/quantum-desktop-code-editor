import React from 'react';
import { render } from 'react-dom';
import Window from './components/QuantumWindow';
import settingsHelper from './settingsHelper';

const root = document.createElement('div');
// Load settings
settingsHelper.loadOnStart();

root.id = 'root';
document.body.appendChild(root);
render(<Window/>, document.getElementById('root'));

