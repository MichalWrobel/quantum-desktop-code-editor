import React from 'react';
import ReactDOM from 'react-dom';
import settingsHelper from '../settingsHelper';
import Lang from '../lang/langUtils';
import Icon from '../components/icons/Icon';
import menuActions from '../components/QuantumMenuActions';
import ButtonSharp from '../components/QuantumButtonSharp';

class Menu extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			activeItem: 'none'
		};
		this.shortKey = process.platform === 'darwin' ? this.shortKey = Lang.translate('cmd') : this.shortKey = Lang.translate('ctrl');
		this.menu = [
			{
				Quantum: {
					login: {
						name: Lang.translate('login'),
						label: Lang.translate('loginDesc'),
						shortcut: ''
					}
				},
			},
			{
				file: {
					new: {
						name: Lang.translate('new'),
						label: Lang.translate('newDesc'),
						shortcut: `${this.shortKey} + N`
					},
					open: {
						name: Lang.translate('open'),
						label: Lang.translate('openDesc'),
						shortcut: `${this.shortKey} + O`
					},
					openFolder: {
						name: Lang.translate('openFolder'),
						label: Lang.translate('openFolderDesc'),
						shortcut: ''
					},
					save: {
						name: Lang.translate('save'),
						label: Lang.translate('saveDesc'),
						shortcut: `${this.shortKey} + S`
					},
					saveAs: {
						name: Lang.translate('saveas'),
						label: Lang.translate('saveasDesc'),
						shortcut: ''
					},
					exit: {
						name: Lang.translate('exit'),
						label: Lang.translate('exitDesc'),
						shortcut: `${this.shortKey} + Q`
					}
				}
			},
			{
				edit: {
					undo: {
						name: Lang.translate('undo'),
						label: Lang.translate('undoDesc'),
						shortcut: `${this.shortKey} + Z`
					}
				}
			},
			{
				find: {
					find: {
						name: `${Lang.translate('find')}...`,
						label: Lang.translate('findDesc'),
						shortcut: `${this.shortKey} + F`
					}
				}
			},
			{
				selection: {
					selectAll: {
						name: Lang.translate('selectall'),
						label: Lang.translate('selectallDesc'),
						shortcut: `${this.shortKey} + A`
					}
				}
			},
			{
				view: {
					pin: {
						name: settingsHelper.read('menuPin') ? Lang.translate('unpin') : Lang.translate('pin'),
						label: Lang.translate('pinDesc'),
						shortcut: ''
					},
					split: {
						name: Lang.translate('split'),
						label: Lang.translate('splitDesc'),
						shortcut: ''
					}
				}
			}
		];
		this.menuRefs = [];
		this.menuLeftOffsets;
		this.menu.forEach((item) => {
			item = React.createRef();
			this.menuRefs.push(item);

		});
	}
	componentDidMount() {
		const offsets = [];
		this.menuRefs.map((item, index) => {
			if (index !== 0) {
				offsets.push(ReactDOM.findDOMNode(this.menuRefs[index].current).getBoundingClientRect().left);
				this.menuLeftOffsets = offsets;
			}
		});
	}
	mouseMenuLeave() {
		this.setState({
			activeItem: 'none'
		});
	}
	hideMenu() {
		const menu = document.getElementById('title-main');
		const editor = document.getElementById('codeEditor');
		if (editor !== null) {
			editor.style.paddingTop = '0px';
			editor.style.transition = `all ${settingsHelper.read('slideOutTime')}s ease-out`;
			editor.style.height = '100%';
		}
		menu.style.marginTop = '-25px';
		menu.style.transition = `all ${settingsHelper.read('slideOutTime')}s ease-out`;
	}
	switchActiveMenu(index, type) {
		if (type === 'click' && index === this.state.activeItem) {
			this.setState({
				activeItem: 'none'
			});

			return;
		}

		this.setState({
			activeItem: index
		});
	}
	switchHoverActiveMenu(index) {
		if (typeof this.state.activeItem === 'string') {

			return;
		}

		this.switchActiveMenu(index);
	}
	getValues() {
		const valuesArray = [];
		if (this.state.activeItem === 'none') {

			return valuesArray;
		}

		const obj = Object.values(Object.values(this.menu[this.state.activeItem])[0]);
		for (let index = 0; index < obj.length; index++) {
			valuesArray.push(Object.values(obj[index]));
		}

		return valuesArray;
	}
	handleAction(index) {

		const amenu = this.state.activeItem;
		const parentObj = this.menu[amenu][Object.keys(this.menu[amenu])];
		const arrayFromObj = [];

		this.setState({
			activeItem: 'none'
		});

		for (const item in parentObj) {
			arrayFromObj.push(item);
		}

		const targetItem = arrayFromObj[index];
		if (targetItem === 'pin') {
			this.menu[amenu].view.pin.name === 'Pin' ?
				this.menu[amenu].view.pin.name = Lang.translate('pin') :
				this.menu[amenu].view.pin.name = Lang.translate('unpin');
		}
		if (targetItem !== 'pin' && !settingsHelper.read('menuPin')) {
			this.hideMenu();
		} else if (targetItem === 'pin' && settingsHelper.read('menuPin')) {

			this.hideMenu();
		}
		menuActions[targetItem]();
	}

	render() {

		const submenuValues = this.getValues();
		const itemId = this.state.activeItem !== 'none' ? Object.keys(this.menu[this.state.activeItem])[0] : -1;
		const leftOffset = this.state.activeItem === 'none' ? 0 : this.menuLeftOffsets[this.state.activeItem - 1];
		let mainClassName = 'menuItem';
		const subClassName = 'submenuItem';

		return(
			<span className = 'menuContainer' onMouseLeave = {this.mouseMenuLeave.bind(this)}>
				<span className = 'menuMain'>
					{
						this.menu.map((menuItem, index) => {
							const quantumMenuActive = this.state.activeItem === 0 ? 'quantumIcon-active' : 'quantumIcon';
							if (index === this.state.activeItem) {
								mainClassName += '-active';
							} else {
								mainClassName = 'menuItem';
							}
							if (index === 0) {

								return (
									<span key = {index} onMouseEnter = {this.switchHoverActiveMenu.bind(this, index, 'hover')} onClick = {this.switchActiveMenu.bind(this, index, 'click')}>
										<span className = 'menuLogoWrapper'>
											<Icon className = {quantumMenuActive} icon = 'logoType' width = '100' height = '25' view = '0 222.5 500 50'/>
										</span>
										<span className = 'logoSeparator'></span>
									</span>
								)
							}

							return (
								<span className = {mainClassName} key = {index} onMouseEnter = {this.switchHoverActiveMenu.bind(this, index, 'hover')} onClick = {this.switchActiveMenu.bind(this, index, 'click')} ref = {this.menuRefs[index]}>
									{Lang.translate(Object.keys(menuItem)[0])}
								</span>
							)
						})
					}
				</span>
				<div className = {itemId === 'Quantum' ? '' : 'subMenu'} active = {itemId} style = {{ left: leftOffset }}>
					{
						submenuValues.map((submenuItem, index) => {
							if (submenuItem[0] === 'Login') {
								// TO DO: User logged in? Return proper element
								return (
									<div key = {index}>
										<div className = 'simpleTriangle' style = {{ marginLeft: leftOffset }}></div>
										<div className = 'quantumSubMenu' onClick = {this.handleAction.bind(this)}>
											<div className = 'quantumSubMenuUser'>You're not logged in</div>
											<ButtonSharp action = '' label = 'Login'/>
										</div>
									</div>
								)
							}

							return (
								<div className = {subClassName} key = {index} onClick = {this.handleAction.bind(this, index)}>
									<div className = 'submenuName'>{submenuItem[0]}</div>
									<div className = 'submenuLabel'>{submenuItem[1]}</div>
									<div className = 'submenuShortcut'>{submenuItem[2]}</div>
								</div>
							)
						})
					}
				</div>
			</span>
		);
	}
}

export default Menu;