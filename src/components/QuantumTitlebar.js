import Icon from '../components/icons/Icon';
import { remote } from 'electron';
import React from 'react';
import '../styles/QuantumTitlebar.css';
import Menu from '../components/QuantumMenu';
class Titlebar extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			menuVisible: false,
		}
	}

	handleMenu() {
		const slidingBar = document.getElementById('title-main');
		requestAnimationFrame ( () => {
			setTimeout( () => {
				slidingBar.classList.remove('Titlebar-main');
				slidingBar.classList.add('Titlebar-main-visible');

				slidingBar.onmouseleave = () => {
				//	slidingBar.classList.remove('Titlebar-main-visible');
				//	slidingBar.classList.add('Titlebar-main');
				}
			}, 1);
			// slidingBar.classList.remove('Titlebar-main-visible');
			// slidingBar.classList.add('Titlebar-main');
		});
	}
	handleIconChange() {
		const currentWindow = remote.getCurrentWindow();
		const icon = this.refs.maxMinIcon;

		if (currentWindow.isMaximized()) {
			currentWindow.restore();

		} else {
			currentWindow.maximize();
		}

		// currentWindow.on('maximize', () => {
		// 	console.log('max');
		// 	icon.setState({ currentIcon: icon.state.icons['restore'] });
		// });
		// currentWindow.on('minimize', () => {
		// 	console.log('min');
		// 	icon.setState({ currentIcon: icon.state.icons['maximize'] });
		// });
	}

	render() {
		const currentWindow = remote.getCurrentWindow();

		return(
			<div className = 'Titlebar'
				onMouseEnter = {this.handleMenu.bind(this)} // <- TO DO
			>
				<div className = 'Titlebar-tiny' ref = 'tiny'></div>
				<div
					id = 'title-main'
					className = 'Titlebar-main'
				>
					<Menu/>
					<Icon className = 'quantumIcon' icon = 'logoType' width = '100' height = '25' view = '0 222.5 500 50'/>
					<div className = 'Titlebar-controlIcons'>
						<span
							className = 'Titlebar-controlIcon'
							onClick = {() => currentWindow.minimize()}>
							<Icon className = 'quantumIcon' icon = 'minimize' view= '-125 -125 750 750'/>
						</span>
						<span
							className = 'Titlebar-controlIcon'
							onClick = {this.handleIconChange.bind(this)}
						>
							<Icon className = 'quantumIcon' icon = { currentWindow.isMaximized() ? 'restore' : 'maximize' } ref = 'maxMinIcon' view= '-125 -125 750 750'/>
						</span>
						<span
							className = 'Titlebar-controlIcon'
							onClick = {() => currentWindow.close()}>
							<Icon className = 'quantumIcon' icon = 'close' view= '-125 -125 750 750'/>
						</span>
					</div>
				</div>
				<div className = 'Titlebar-thickLine'></div>
			</div>
		)
	}
}

export default Titlebar;