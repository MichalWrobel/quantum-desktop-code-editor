import { remote } from 'electron';
import settingsHelper from '../settingsHelper';
import React from 'react';
import Icon from '../components/icons/Icon';
import Menu from '../components/QuantumMenu';
import menuActions from '../components/QuantumMenuActions';
import CodeArea from '../components/QuantumCodeArea';
import Lang from '../lang/langUtils';
import ButtonSharp from '../components/QuantumButtonSharp';
import '../styles/QuantumWindow.css';
import '../styles/QuantumTitlebar.css';
import '../styles/QuantumMenu.css';

class Window extends React.Component {

	constructor(props) {
		super(props);
		this.defaults = {
			markdown: '',
			javascript: ''
		};
		this.state = {
			isMinimized: remote.getCurrentWindow().isMaximized(),
			menuVisible: false,
			code: this.defaults.markdown,
			readOnly: false,
			mode: 'markdown',
			activeDocs: []
		};
		this.slideRef = React.createRef();
		this.editor = React.createRef();
		this.codeEditor = React.createRef();
	}

	closeDoc(name) {
		const entry = this.state.activeDocs.indexOf(name);
		this.state.activeDocs.splice(entry, 1);
		settingsHelper.memoryServer.forEach((entry) => {
			if(entry[0] === name) {
				settingsHelper.memoryServer.splice(entry, 1);
			}
		})
	}
	updateCode(newCode) {
		this.setState({
			code: newCode
		});
	}
	changeMode(e) {
		var mode = e.target.value;
		this.setState({
			mode: mode,
			code: this.defaults[mode]
		});
	}
	toggleReadOnly() {
		this.setState({
			readOnly: !this.state.readOnly
		}, () => this.refs.editor.focus());
	}

	componentDidMount() {
		const currentWindow = remote.getCurrentWindow();
		const icon = this.refs.maxMinIcon;
		if (!this.state.activeDocs.length) {
			this.welcomeScreenBgEffect();
		}

		currentWindow.on('maximize', () => {
			icon.setState({ currentIcon: icon.state.icons['restore'] });
		});
		currentWindow.on('unmaximize', () => {
			icon.setState({ currentIcon: icon.state.icons['maximize'] });
		});
	}

	handleMenu() {

		let visibleMenu = false;
		const topMargin = this.slideRef.current.style.marginTop;
		const slideOutTime = settingsHelper.read('slideOutTime');
		const slideInTime = settingsHelper.read('slideInTime');
		const menuPin = settingsHelper.read('menuPin');

		if (topMargin === '5px' && !menuPin) {
			this.slideRef.current.style.transition = `all ${slideOutTime}s ease-out`;
			this.slideRef.current.style.marginTop = '-30px';
			if(this.state.activeDocs.length) {
				this.editor.current.style.transition = `all ${slideOutTime - 0.5}s ease-out`;
				this.editor.current.style.paddingTop = '0px';
				this.editor.current.style.height = '100%';
			}
		} else {
			this.slideRef.current.style.transition = `all ${slideInTime}s ease-out`;
			this.slideRef.current.style.marginTop = '5px';
			if(this.state.activeDocs.length) {
				this.editor.current.style.transition = `all ${slideInTime}s ease-out`;
				this.editor.current.style.paddingTop = '30px';
				this.editor.current.style.height = 'calc(100% - 30px)';
			}
			visibleMenu = true;
		}

		this.setState({ menuVisible: visibleMenu });
	}

	handleIconChange() {
		const currentWindow = remote.getCurrentWindow();

		if (currentWindow.isMaximized()) {
			currentWindow.restore();

		} else {
			currentWindow.maximize();
		}
	}
	closeWindow() {
		setTimeout(settingsHelper.writeFromMemory(), 1);
		remote.getCurrentWindow().close();
	}
	welcomeScreenBgEffect() {
		const colors = ['#444444', '#2b2b2b', '#BBBBBB', '#0a4c9e', '#6d059a'];

		const numBalls = 25;
		const balls = [];

		for (let i = 0; i < numBalls; i++) {
			const ball = document.createElement('div');
			ball.classList.add('ball');
			ball.style.background = colors[Math.floor(Math.random() * colors.length)];
			ball.style.left = `${Math.floor(Math.random() * 100)}vw`;
			ball.style.top = `${Math.floor(Math.random() * 100)}vh`;
			ball.style.transform = `scale(${Math.random()})`;
			ball.style.width = `${Math.random()}em`;
			ball.style.height = ball.style.width;

			balls.push(ball);
			document.getElementsByClassName('Welcome-screen')[0].append(ball);
		}

		balls.forEach((el, i) => {
			const to = {
				x: Math.random() * (i % 2 === 0 ? -11 : 11),
				y: Math.random() * 12
			};

			el.animate(
				[
					{ transform: 'translate(0, 0)' },
					{ transform: `translate(${to.x}rem, ${to.y}rem)` }
				],
				{
					duration: (Math.random() + 1) * 2000, // random duration
					direction: 'alternate',
					fill: 'both',
					iterations: Infinity,
					easing: 'ease-in-out'
				}
			);
		});

	}
	handleActions(action) {
		const valuesFromAction = menuActions[action]();
		if(action === 'open') {
			
		}
		console.log(valuesFromAction)
	}

	render() {
		const currentWindow = remote.getCurrentWindow();

		return (
			<div>
				<div className = 'Titlebar'
					onMouseEnter = {this.handleMenu.bind(this)} // <- TO DO
					onMouseLeave = {this.handleMenu.bind(this)}
				>
					<div className = 'Titlebar-tiny' ref = 'tiny'></div>
					<div
						id = 'title-main'
						className = 'Titlebar-main'
						ref = {this.slideRef}
					>
						<Menu />
						<div className = 'Titlebar-controlIcons'>
							<span
								className = 'Titlebar-controlIcon'
								onClick = {() => currentWindow.minimize()}>
								<Icon className = 'quantumIcon' icon = 'minimize' view= '-125 -125 750 750'/>
							</span>
							<span
								className = 'Titlebar-controlIcon'
								onClick = {this.handleIconChange.bind(this)}
							>
								<Icon className = 'quantumIcon' icon = { currentWindow.isMaximized() ? 'restore' : 'maximize' } ref = 'maxMinIcon' view= '-125 -125 750 750'/>
							</span>
							<span
								className = 'Titlebar-controlIcon'
								onClick = {() => this.closeWindow()}>
								<Icon className = 'quantumIcon' icon = 'close' view= '-125 -125 750 750'/>
							</span>
						</div>
						{ this.state.activeDocs.length > 0 &&

							<span className = 'Tab-controller'>
								<span className = 'Tab-controller-name'> {`${Lang.translate('active')}: ${this.state.activeDocs[0]}`}</span>
								<span onClick = { this.closeDoc.bind(this, this.state.activeDocs[0]) } className = 'Tab-controller-closeIcon'>
									<Icon icon = 'close' view= '-225 -225 950 950'/>
								</span>
							</span>
						}
					</div>
				</div>
				{ this.state.activeDocs.length ?
					(<div id = 'codeEditor'>
						<CodeArea ref = {this.codeEditor} value = { this.state.code }/>
					</div>) :
					(<div className = 'Welcome-screen'>
						<div className = 'Welcome-screen-background'>
							{Lang.translate('welcomeTitle')}
							<div className = 'Welcome-screen-hints'>{Lang.translate('newDesc')}</div>
							<ButtonSharp action = '' label = {Lang.translate('new')}/>
							<div className = 'Welcome-screen-hints'>{Lang.translate('openDesc')}</div>
							<ButtonSharp action = {this.handleActions.bind(this, 'open')} label = {Lang.translate('open')}/>
							<div className = 'Welcome-screen-hints'>{Lang.translate('exitDesc')}</div>
							<ButtonSharp action = 'exit' label = {Lang.translate('exit')}/>
						</div>
					</div>)
				}
			</div>
		)
	}
}

export default Window;
