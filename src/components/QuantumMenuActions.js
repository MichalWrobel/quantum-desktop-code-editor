import settingsHelper from '../settingsHelper';
import { remote } from 'electron';
import fs from 'fs';

const fileFilters = [
	// TO DO: TRANSLATE ! ! !
	{ name: 'All files', extensions: ['*'] },
	{ name: 'Scripts', extensions: ['js', 'htm', 'html'] }
]

const stringCut = (wantedString, fromChar) => {
	const lastChar = wantedString.lastIndexOf(fromChar) + 1

	return wantedString.slice(lastChar);
}

const menuActions = {
	pin: () => {
		const currentSet = settingsHelper.read('menuPin');
		settingsHelper.setValue(!currentSet, 'menuPin');
	},
	open: () => {
		const fak = remote.dialog.showOpenDialog({ filters: fileFilters }, (fileNames) => {
			if(fileNames === undefined) {

				return;
			}

			const openedFileName = stringCut(fileNames[0], '\\');
			const fileExtension = stringCut(fileNames[0], '.');

			const fileData = function() {
				const shit = fs.readFileSync(fileNames[0], 'utf-8', (err, data) => {
					if(err) {
						console.errror(`ERROR: File system error #6 (open): ${err.message}`);

						return;
					}
					//settingsHelper.memoryServer.push([openedFileName, fileExtension, data]);

					return settingsHelper.memoryServer.push([openedFileName, fileExtension, data]);
				});

				return shit;
			}

			//console.log([openedFileName, fileExtension, fileData()]);
			return fileData();
		});

		console.log('fak', fak)
		// return settingsHelper.memoryServer.push([openedFileName, fileExtension, fileData()]);
	},
	exit: () => {
		setTimeout(settingsHelper.writeFromMemory(), 1);
		remote.getCurrentWindow().close();
	}
};

export default menuActions;