import React from 'react';
import { PropTypes } from 'prop-types';
import menuActions from '../components/QuantumMenuActions';
import '../styles/QuantumButtonSharp.css';

class ButtonSharp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			buttonActive: true
		};
	}
	handleAction() {
		this.props.action();
	}
	render() {

		return(
			<div
				className = 'ButtonSharpActive'
				action = ''
				onClick = { this.handleAction.bind(this) }
			>
				<div className = 'ButtonSharpActive-inner'>
					<div className = 'ButtonSharpActive-label'>{this.props.label}</div>
				</div>
			</div>
		)
	}
}

ButtonSharp.propTypes = {
	label: PropTypes.string.isRequired
};

ButtonSharp.defaultProps = {
	label: 'Button'
};

export default ButtonSharp;