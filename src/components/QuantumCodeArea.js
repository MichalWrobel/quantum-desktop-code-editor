import React from 'react';
import { Controlled as CodeMirror } from 'react-codemirror2';
import settingsHelper from '../settingsHelper';
import '../styles/QuantumCodeArea.css';
// import 'codemirror/lib/codemirror.css';
import '../styles/cmain/themes/quantum.css';
// import '../styles/cmain/themes/*';
import '../styles/cmain/themes/neat.css';
import '../styles/cmain/codemirror.css';
import '../styles/cmain/simplescrollbars.css';
// import 'codemirror/theme/material.css';
// import 'codemirror/theme/neat.css';
import 'codemirror/mode/xml/xml.js';
import 'codemirror/mode/javascript/javascript.js';
// COMMENT
import 'codemirror/addon/comment/comment.js';
import 'codemirror/addon/comment/continuecomment.js';
// FOLD
import 'codemirror/addon/fold/foldgutter.js';
import 'codemirror/addon/fold/foldcode.js';
import 'codemirror/addon/fold/brace-fold.js';
// SCROLL
import 'codemirror/addon/scroll/scrollpastend.js';
import 'codemirror/addon/scroll/annotatescrollbar.js';
import 'codemirror/addon/scroll/simplescrollbars.js';

// EDIT
import 'codemirror/addon/edit/closebrackets.js';
import 'codemirror/addon/edit/matchbrackets.js';
import 'codemirror/addon/edit/closetag.js';
import 'codemirror/addon/edit/matchtags.js';
import 'codemirror/addon/edit/trailingspace.js';

class CodeArea extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: this.props.value
		};
		this.options = settingsHelper.memoryEditorSettings;
	}

	render() {

		return (
			<div className = 'CodeArea'>
				<CodeMirror
					value = {this.state.value}
					options = {this.options}
					onBeforeChange = {(editor, data, value) => {
						this.setState({ value });
					}}
					autoFocus = {true}
					autoScroll = {true}
				/>
			</div>
		)
	}
}

export default CodeArea;